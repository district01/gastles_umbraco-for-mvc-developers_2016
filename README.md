# Umbraco for MVC developers #

Getting this project running takes only 5 steps:

1. Clone this repository to your local disk
2. Open the Visual Studio Project in /umbraco/BurgersDemo.sln
3. Using Visual Studio do a right click on the solution name in Solution Explorer window and select "Manage Nuget packages for solution". 
4. In the dialog popping op, click "Restore" in the yellow notification
5. Hit F5 or Build and run the applications.

Questions? 

Contact [Mante Bridts](mailto:mante.bridts@district01.be).
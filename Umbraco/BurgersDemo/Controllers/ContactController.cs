﻿using System.Net.Mail;
using System.Web.Mvc;
using BurgersDemo.Models;
using Umbraco.Web.Mvc;

namespace BurgersDemo.Controllers
{
    public class ContactController : SurfaceController
    {
        private const string MailBody = @"Beste,

Er heeft iemand het contactformulier van de website ingevuld:
Naam: {0}
Email: {1},
Bericht:
{2}";


        [HttpPost]
        public PartialViewResult Submit(ContactModel model)
        {
            if (ModelState.IsValid)
            {
                var mail = new MailMessage();
                mail.From = new MailAddress(model.Email);
                mail.To.Add("info@district01.be");
                mail.Subject = "Nieuw bericht via de website";
                mail.Body = string.Format(MailBody, model.Name, model.Email, model.Message);
                try
                {
                    new SmtpClient().Send(mail);
                }
                catch
                {
                    return PartialView("ContactError", model);
                }
                return PartialView("ContactSuccess", model);
            }
            return PartialView("ContactError", model);
        }
    }
}
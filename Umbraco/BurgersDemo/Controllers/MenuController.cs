﻿using BurgersDemo.Models;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace BurgersDemo.Controllers
{
    public class MenuController : SurfaceController
    {
        // GET: Menu
        public ActionResult Index()
        {

            //Voorbeeld om deze op te halen uit de DB. Aangezien deze data momenteel niet in de SQL Server Compact DB zit, gebruiken we onderstaande code die de items zelf aanmaakt en doorgeeft

            //var db = ApplicationContext.DatabaseContext.Database;
            //var menuItems = db.Fetch<MenuItem>(new Sql().Select("*").From("Menu"));


            var menuItems = new[]
            {
                new MenuItem {Id = 1, Name = "Blue Cheese Madness", Description = "100% rundergehakt, Bleu de Causses, raketsla, rode uienconfituur", Price = 4.2M},
                new MenuItem {Id = 2, Name = "Smoked Salmon", Description = "Zalmfilet, gerookte zalmmousse, raketsla, sla, Gravad Lax", Price = 5.5M},
                new MenuItem {Id = 3, Name = "Meatlover by Dierendonck", Description = "100% rundergehakt van Dierendonck, sla, tomaat, rode ui, augurken, mosterd en ketchup", Price = 4.5M},
                new MenuItem {Id = 4, Name = "Funky Chicken", Description = "Kipfilet, gegrilde groenten, dragonmayonaise, rode ui, parmezaanschilfers", Price = 4.6M},
                new MenuItem {Id = 5, Name = "Lovely Lamb with spoon", Description = "Lam, Torta de Oveja, raketsla, sla, paprikatapenade", Price = 4.8M}
            };
            
            return Json(menuItems, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿var franchise = {};
franchise.init = {};

$(function () {
    franchise.init.carousel = function () {
        var clickEvent = false;
        $('#myCarousel').carousel({
            interval: 4000
        });

        $('#myCarousel').on('click', '.nav a', function () {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(this).parent().addClass('active');
        }).on('slid.bs.carousel', function (e) {
            if (!clickEvent) {
                var count = $('#myCarousel .nav').children().length - 1;
                console.log(count);
                var current = $('#myCarousel .nav li.active');
                current.removeClass('active').next().addClass('active');
                var id = parseInt(current.data('slide-to'));
                if (count == id) {
                    $('#myCarousel .nav li').first().addClass('active');
                }
            }
            clickEvent = false;
        });
    };

    franchise.init.jobs = function () {
        $('.collapse').on('show.bs.collapse', function () {
            var id = $(this).attr('id');
            $('a[href="#' + id + '"]').closest('.panel-heading').addClass('active-faq');
            $('a[href="#' + id + '"] .panel-title span').html('<img src="images/jobs-active.png" />');
        });
        $('.collapse').on('hide.bs.collapse', function () {
            var id = $(this).attr('id');
            $('a[href="#' + id + '"]').closest('.panel-heading').removeClass('active-faq');
            $('a[href="#' + id + '"] .panel-title span').html('<img src="images/jobs-inactive.png" />');
        });
    };

    franchise.init.locaties = function () {
        var jsonLocaties = {
          "locaties": [
            {
              "locatieNaam": "Antwerpen-Centraal",
              "image": "images/antwerpen.jpg",
              "adres": {
                "straat": "Koning Astridplein 23",
                "postcode": "2060",
                "stad": "Antwerpen"
              }
            },
            {
              "locatieNaam": "Brussel-Noord",
              "image": "images/brussel.jpg",
              "adres": {
                "straat": "Rue De Loi 34",
                "postcode": "1000",
                "stad": "Brussel"
              }
            }
          ]
        };

        $('#locations-list').append(
    $.map(jsonLocaties.locaties, function (locatie, index) {
        return "<article class='col-xs-6 col-md-3 location-item'><div class='image-container'><img src='" + locatie.image + "' /></div><header><h3>" + locatie.locatieNaam + "</h3></header><p>" + locatie.adres.straat + " - " + locatie.adres.postcode + " " + locatie.adres.stad + "</p><a href='" + locatie.locatieNaam + "' title='" + locatie.locatieNaam + "'>Bekijk detail</a></article>";
        }).join());

    };

    franchise.init.contact = function () {
        function initialize() {
            var myLatlng = new google.maps.LatLng(51.141888, 4.442956);
            var mapOptions = {
                zoom: 14,
                center: myLatlng
            }
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Hello World!'
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    };

    //franchise.init.location = function () {
    //    var map;
    //    var service;
    //    var infowindow;
    //    var teller = 0;
    //    document.getElementsByTagName
    //    var carousel_inner = $('.carousel-inner').children('div').children('div').children('img');
    //    var carousel_nav = $('.carousel-nav').children('li').children('a').children('img');

    //    function initialize() {
    //        var myLatlng = new google.maps.LatLng(51.219247, 4.396246);
    //        var mapOptions = {
    //            zoom: 14,
    //            center: myLatlng
    //        }
    //        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    //        var marker = new google.maps.Marker({
    //            position: myLatlng,
    //            map: map,
    //            title: 'Hello World!'
    //        });

    //        var request = {
    //            location: myLatlng,
    //            radius: '500',
    //            types: ['store']
    //        };

    //        service = new google.maps.places.PlacesService(map);
    //        service.nearbySearch(request, callback);
    //    }

    //    function callback(results, status) {
    //        if (status == google.maps.places.PlacesServiceStatus.OK) {
    //            for (var i = 0; i < results.length; i++) {
    //                addPhoto(results[i]);
    //            }
    //        }
    //    }

    //    function addPhoto(place) {
    //        var photos = place.photos;
    //        if (!photos) {
    //            return;
    //        }

    //        if (teller < 4) {
    //            carousel_inner[teller].src = photos[0].getUrl({ 'maxWidth': 555, 'maxHeight': 380 });
    //            carousel_nav[teller].src = photos[0].getUrl({ 'maxWidth': 555, 'maxHeight': 380 });
    //            teller += 1;
    //            $('#myCarousel').fadeIn();
    //        }

    //    }

    //    google.maps.event.addDomListener(window, 'load', initialize);
        
    //}

    franchise.init.location = function (latitude, longitude) {
        var gmap;
        var marker;
        function initialize() {

            gmap = new google.maps.Map(document.getElementById('map-canvas'), {
                center: { lat: parseFloat(latitude), lng: parseFloat(longitude) },
                scrollwheel: false,
                zoom: 14,
                mapTypeControl: false,
                streetViewControl: false
            });
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude),
                map: gmap
            });
        }

        $(window).load(function () {
            // Create a map object and specify the DOM element for display.
            initialize();
            $(window).resize(function () {
                initialize();
            });
        });
    }
});



